package com.hiro.simple2dphysicsengine;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;

import com.hiro.simple2dphysicsengine.graphics.DisplayView;
import com.hiro.simple2dphysicsengine.physics_engine.Environment;

/**
 * Main activity for the app
 */
public class MainActivity extends AppCompatActivity
{
    DisplayView displayView;
    Environment environment;

    /**
     * Set up the state
     * @param savedInstanceState not used
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Lock orientation
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);

        // Set up the environment
        environment = new Environment(0.3f);   // arbitrary dt value

        // Set up display view
        displayView = findViewById(R.id.canvas);
        displayView.setEnvironment(environment);

        // Set up sensors
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor gravitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);

        // Create a listener
        SensorEventListener gravitySensorListener = new SensorEventListener()
        {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent)
            {
                float x = sensorEvent.values[0];
                float y = sensorEvent.values[1];

                // Update the state of the environment
                environment.setGravity(-x, y);  // y-axis is inverted
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) { }
        };

        // Register the listener
        sensorManager.registerListener(
                gravitySensorListener,
                gravitySensor,
                SensorManager.SENSOR_DELAY_GAME);

        displayView.start();
    }

    /**
     * Inflate the menu
     * @param menu menus
     * @return true
     */
    public boolean onCreateOptionsMenu(Menu menu)
    {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    /**
     * Open an alert dialog to set parameters
     * @param item not used
     * @return true
     */
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Set up alert dialog
        LayoutInflater inflater = this.getLayoutInflater();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View layout = inflater.inflate(R.layout.settings, null);
        builder.setView(layout);
        builder.setMessage("Set parameters");
        builder.setCancelable(true);

        // Get the seekBars
        final SeekBar gravityBar    = layout.findViewById(R.id.gravity_seekBar);
        final SeekBar corBar        = layout.findViewById(R.id.cor_seekBar);
        final SeekBar resistanceBar = layout.findViewById(R.id.resistance_seekBar);
        final SeekBar radiusBar     = layout.findViewById(R.id.radius_seekBar);

        // Set progresses
        gravityBar.setProgress((int) (environment.getGravityCoefficient() * gravityBar.getMax() / 10));
        corBar.setProgress((int) (environment.getCor() * corBar.getMax()));
        resistanceBar.setProgress((int)(environment.getResistanceCoefficient() * resistanceBar.getMax()));
        radiusBar.setProgress((int) environment.getRadius());

        // OK clicked
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                double gravityCoefficient = (double) gravityBar.getProgress() / (double) gravityBar.getMax() * 10;
                environment.setGravityCoefficient(gravityCoefficient);

                double cor = (double)  corBar.getProgress() / (double) corBar.getMax();
                environment.setCor(cor);

                double resistance = (double)  resistanceBar.getProgress() / (double) resistanceBar.getMax();
                environment.setResistanceCoefficient(resistance);

                double radius = (double) radiusBar.getProgress();
                environment.setRadius(radius);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                // Do nothing
            }
        });

        builder.create();
        builder.show();
        return true;
    }
}
