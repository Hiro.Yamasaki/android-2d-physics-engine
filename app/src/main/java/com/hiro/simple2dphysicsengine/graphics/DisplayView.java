package com.hiro.simple2dphysicsengine.graphics;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.hiro.simple2dphysicsengine.physics_engine.Environment;
import com.hiro.simple2dphysicsengine.physics_engine.Particle;
import com.hiro.simple2dphysicsengine.physics_engine.Vector;

import static java.lang.Thread.sleep;

/**
 * Responsible for displaying the current state
 */
public class DisplayView extends SurfaceView implements SurfaceHolder.Callback
{
    /* Member variables ***********************************************************************************************/

    private SurfaceHolder holder;
    private Environment environment;
    private Paint particlePaint;
    private boolean running = false;

    /* Constructors ***************************************************************************************************/

    public DisplayView(Context context)
    {
        super(context);
        init();
    }

    public DisplayView(Context context, AttributeSet attributeSet)
    {
        super(context, attributeSet);
        init();
    }

    /* Public methods *************************************************************************************************/

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder)
    {
        environment.setBorders(getHeight(), getWidth());
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) { }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) { }

    /**
     * Start running the animation
     */
    public void start()
    {
        running = true;
        drawOnThread();
    }

    /**
     * Stop animation
     */
    public void stop()
    {
        running = false;
    }

    /* Private methods ************************************************************************************************/

    /**
     * Initialization
     */
    private void init()
    {
        holder = getHolder();
        holder.addCallback(this);

        particlePaint = new Paint();
    }


    /**
     * Main drawing thread
     */
    private void drawOnThread()
    {
        Thread t = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                while (running)
                {
                    Canvas canvas = null;
                    try
                    {
                        canvas = holder.lockCanvas(null);

                        environment.update();

                        if (canvas != null)
                            drawParticles(canvas);

                        sleep(30);

                    } catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                    finally
                    {
                        if (canvas != null)
                            holder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        });

        t.start();
    }

    /**
     * Get state from the environment and draw
     * @param canvas canvas to draw on
     */
    private void drawParticles(Canvas canvas)
    {
        canvas.drawColor(Color.WHITE);  // clear

        Particle particle = environment.getParticle();
        Vector position = particle.getPosition();
        float radius = (float) particle.getRadius();

        canvas.drawCircle((float) position.x, (float) position.y, radius, particlePaint);
    }

    /* Getters and setters ********************************************************************************************/

    public void setEnvironment(Environment environment)
    {
        this.environment = environment;
    }
}
