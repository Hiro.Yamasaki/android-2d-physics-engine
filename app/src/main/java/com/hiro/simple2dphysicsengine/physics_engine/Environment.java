package com.hiro.simple2dphysicsengine.physics_engine;

/**
 * Represents the virtual environment
 */
public class Environment
{
    /* Member variables ***********************************************************************************************/

    private double dt;  // time step
    private Particle particle;
    private Vector gravity = new Vector(0.f, 0.f);
    private double gravityCoefficient = 1;

    // Default screen size (to be overridden by DisplayView class)
    private double bottom = 2000;
    private double right  = 2000;

    private double cor = 0.9; // Coefficient of Restitution (COR)
    private double resistanceCoefficient = 0.0001;   // max 0.01

    /* Constructors ***************************************************************************************************/

    public Environment(double dt)
    {
        this.dt = dt;
        this.particle = new Particle(10, 1);
    }

    /* Public methods *************************************************************************************************/

    /**
     * Update to get the state in the next time step
     */
    public void update()
    {
        calcState();

        checkBorderCollision();
    }

    /* Private methods ************************************************************************************************/

    /**
     * Calculate the position and the velocity
     */
    private void calcState()
    {
        // Get force and convert to acceleration
        Vector acceleration = getForce().multiply(1 / particle.getMass());

        // Update velocity
        particle.setVelocity(calcVelocity(particle.getVelocity(), acceleration));

        // Update position
        particle.setPosition(calcPosition(particle.getPosition(), particle.getVelocity()));
    }


    /**
     * Check if the particle has collided with the border
     */
    private void checkBorderCollision()
    {
        // Right border
        if (particle.getPosition().x + particle.getRadius() > right)
        {
            particle.getPosition().x = right - particle.getRadius();

            double x = particle.getVelocity().x;
            double y = particle.getVelocity().y;
            particle.setVelocity(new Vector(-x * cor, y));
        }

        // Left border
        if (particle.getPosition().x - particle.getRadius() < 0)
        {
            particle.getPosition().x = particle.getRadius();

            double x = particle.getVelocity().x;
            double y = particle.getVelocity().y;
            particle.setVelocity(new Vector(-x * cor, y));
        }

        // Top border
        if (particle.getPosition().y - particle.getRadius() < 0)
        {
            particle.getPosition().y = particle.getRadius();

            double x = particle.getVelocity().x;
            double y = particle.getVelocity().y;
            particle.setVelocity(new Vector(x, -y * cor));
        }

        // Bottom border
        if (particle.getPosition().y + particle.getRadius() > bottom)
        {
            particle.getPosition().y = bottom - particle.getRadius();

            double x = particle.getVelocity().x;
            double y = particle.getVelocity().y;
            particle.setVelocity(new Vector(x, -y * cor));
        }
    }


    /**
     * Calculate the force
     * @return force
     */
    private Vector getForce()
    {
        // Resistance proportional to velocity
        Vector resistance = new Vector(-particle.getVelocity().x, -particle.getVelocity().y);
        resistance.multiply(resistanceCoefficient);

        // Gravity
        Vector force =  new Vector(
                gravityCoefficient * gravity.x * particle.getMass(),
                gravityCoefficient * gravity.y * particle.getMass());

        // Total force
        force = Vector.add(force, resistance);

        return force;
    }

    /**
     * Calculate velocity from acceleration
     * @param velocity old velocity
     * @param acceleration acceleration
     * @return new velocity
     */
    private Vector calcVelocity(Vector velocity, Vector acceleration)
    {
        // v + a x dt
        return velocity.add(acceleration.multiply(dt));
    }

    /**
     * Calculate position from velocity
     * @param position old position
     * @param velocity velocity
     * @return new position
     */
    private Vector calcPosition(Vector position, Vector velocity)
    {
        // p + v x dt
        return position.add(Vector.multiply(velocity, dt));
    }

    /**
     * Clamp position inside the borders
     */
    private void clampPositions()
    {
        particle.getPosition().x = Math.max(particle.getRadius(),
                Math.min(right - particle.getRadius(), particle.getPosition().x));
        particle.getPosition().y = Math.max(particle.getRadius(),
                Math.min(bottom - particle.getRadius(), particle.getPosition().y));
    }

    /* Getters and setters ********************************************************************************************/

    public Particle getParticle()
    {
        return particle;
    }

    public void setGravity(float x, float y)
    {
        gravity.x = x;
        gravity.y = y;
    }

    public void setBorders(double bottom, double right)
    {
        this.bottom = bottom;
        this.right = right;
        clampPositions();
    }

    public void setCor(double cor)
    {
        this.cor = cor;
    }

    public double getCor()
    {
        return cor;
    }

    public void setGravityCoefficient(double gravityCoefficient)
    {
        this.gravityCoefficient = gravityCoefficient;
    }

    public double getGravityCoefficient()
    {
        return gravityCoefficient;
    }

    public void setResistanceCoefficient(double resistanceCoefficient)
    {
        this.resistanceCoefficient = resistanceCoefficient;
    }

    public double getResistanceCoefficient()
    {
        return resistanceCoefficient;
    }

    public void setRadius(double radius)
    {
        particle.setRadius(radius);

        // Make sure it still fit inside the borders
        if (particle.getPosition().x - radius < 0)
        {
            particle.getPosition().x = radius;
        }
        if (particle.getPosition().x + radius > bottom)
        {
            particle.getPosition().x = bottom - radius;
        }
        if (particle.getPosition().y - radius < 0)
        {
            particle.getPosition().y = radius;
        }
        if (particle.getPosition().y + radius > right)
        {
            particle.getPosition().y = right - radius;
        }
    }

    public double getRadius()
    {
        return particle.getRadius();
    }

}
