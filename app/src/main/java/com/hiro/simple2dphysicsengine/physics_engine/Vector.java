package com.hiro.simple2dphysicsengine.physics_engine;

/**
 * Generic vector class
 */
public class Vector
{
    /* Member variables ***********************************************************************************************/

    public double x;
    public double y;

    /* Constructors ***************************************************************************************************/
    public Vector() { }

    public Vector(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    public Vector(Vector vector)
    {
        this.x = vector.x;
        this.y = vector.y;
    }

    /* Static methods *************************************************************************************************/

    static public Vector add(Vector v1, Vector v2)
    {
        return new Vector(v1.x + v2.x, v1.y + v2.y);
    }

    static public Vector subtract(Vector v1, Vector v2)
    {
        return new Vector(v1.x - v2.x, v1.y - v2.y);
    }

    static public Vector multiply(Vector v, double value)
    {
        return new Vector(v.x * value, v.y * value);
    }

    /* Public methods *************************************************************************************************/

    public double abs()
    {
        return (double) Math.sqrt((double) (x*x + y*y));
    }

    public Vector add(Vector vector)
    {
        x += vector.x;
        y += vector.y;
        return this;
    }

    public Vector multiply(double value)
    {
        x *= value;
        y *= value;
        return this;
    }

    /**
     * Return a vector with same direction and length 1
     * @return norm
     */
    public Vector norm()
    {
        Vector vector = new Vector(this);
        if (abs() != 0)
        {
            vector.multiply(1 / abs());
            return vector;
        }
        else
        {
            return vector;
        }
    }
}
