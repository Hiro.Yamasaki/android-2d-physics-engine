This is an app used to test out the gravity sensor feature in Android devices.
It takes x and y components of the gravity and applies it to a single particle
inside a simple 2D physics engine. You can tilt the device to apply different
amount of gravitational force on the particle. The particle is confined to the
size of the screen. You can set various parameters from the 
menu (gravity strength, coefficient of restitution, radius of the particle,
resistance)

<img src="app/data/2dphysics.gif"  width="200" height="400">